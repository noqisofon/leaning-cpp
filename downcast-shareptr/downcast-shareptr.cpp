﻿#include <iostream>
#include <memory>

#include "Derived.hxx"

int main() {
    auto base       = std::shared_ptr<Base>( new Derived() );
    auto derived    = std::dynamic_pointer_cast<Derived>( base );

    return 0;
}